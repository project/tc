Tinycontrol
===========

Allows [Tinycontrol](http://tinycontrol.eu/) to submit data to a Drupal site.

Project site: http://drupal.org/project/tc

Related blog post: (to be released)


Installation
------------

Installing this module needs some additional steps to the [generic
installation](https://www.drupal.org/documentation/install/modules-themes/modules-8).

1. Download [Chart.js](http://www.chartjs.org/) from
[Github](https://raw.githubusercontent.com/nnnick/Chart.js/master/Chart.js).

2. Put it into the `js` directory next to `tc.js`, so you will have
`modules/tc/js/Chart.js` and `modules/tc/js/tc.js`.

3. Enable the module.
